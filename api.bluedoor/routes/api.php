<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('newuser','UserController@store');
Route::post('login', 'AuthController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('users','UserController@index');
    Route::post('updateuser','UserController@updateuser');
    
});
Route::get('cities','CityController@index');
Route::get('states','StateController@index');
Route::get('contries','ContryController@index');
Route::post('createDatabase','CountryController@createCountryStateCityData');

