<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    protected $fillable = ['roomid', 'personid', 'details', 'created_at', 'updated_at', 'address', 'number', 'contryid', 'neighborhood', 'complement', 'stateid', 'cityid', 'zipcode'];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'rooms';

    public function Person(){
        return $this->hasMany('App\Room');
    }
}
