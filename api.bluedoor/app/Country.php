<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = ['contryid', 'countryname'];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'countries';

    public function Person(){
        return $this->hasMany('App\Country');
    }
}

//INSERT INTO countries VALUES(1,'Brasil','BRA','BR','55','Brasilia','BRL','pt')