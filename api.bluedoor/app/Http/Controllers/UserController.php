<?php

namespace App\Http\Controllers;

use App\User;
use App\Person;
use App\Email;
use Illuminate\Http\Request;
use Hash;
class UserController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['accountstatus'] = false;
        try{
            $newPerson = Person::create($data);
            $data['personid'] = $newPerson->personid;
        }catch(\Exception $e){
            return response()->json(['Status'=>false,'msg'=>'Falha ao cadastrar, atualize a página e tente novamente','Erro'=>$e],400);
        }
        try{
            if($newPerson){
                $newUser = User::create($data);
            }
            else{
                $newPerson->delete();
                return response()->json(['Status'=>false,'msg'=>'Falha ao cadastrar usuário, atualize a página e tente novamente'],400);
            }
        }catch(\Exception $e){
            $newPerson->delete();
            return response()->json(['Status'=>false,'msg'=>'Falha ao cadastrar usuário, atualize a página e tente novamente'],400);
        }
        try{
            $email = Email::create($data);
            return response()->json(['Status'=>true,'msg'=>'Usuário cadastrado com sucesso!']);
        }catch(\Exception $e){
            $newUser->delete();
            $newPerson->delete();
            return response()->json(['Status'=>false,'msg'=>'Email já cadastrado'],400);
        }
    }

   public function updateuser(Request $request){
        $data = $request->all();
        if(array_key_exists("password", $data )){
            $data['password'] = Hash::make($data['password']);
        }
        $updatedUser = User::where($data['userid'],'=','userid')->update($data);
        if($updatedUser){
            return response()->json(['Status'=>true,'msg'=>'Usuário atualizado com sucesso!']);
        }else{
            return response()->json(['Status'=>false,'msg'=>'Falha ao alterar usuário, por favor, tente novamente!']);
        }
   }
}
