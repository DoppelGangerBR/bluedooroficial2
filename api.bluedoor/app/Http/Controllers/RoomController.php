<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
class RoomController
{
    //
    public function store(Request $request){
        $newRoom = $request->all();
        $roomCreated = Room::create($newRoom);
        if($roomCreated){
            return response()->json(['Status'=>true]);
        }else{
            return response()->json(['Status'=>false]);
        }
    }
}
