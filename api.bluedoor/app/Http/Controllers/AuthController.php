<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Hash;
use JWTAuth;
use JWTGuard;

class AuthController
{
    //
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email','password');
        $user = User::where('email', $credentials['email'])->first();
        if(!$user){
            return response()->json([
                'Erro' => 'Usuário não encontrado'
            ], 401);
        }
        if($user['status'] == false){
            return response()->json([
                'Erro' => 'user desativado, por favor, contate-nos'
            ], 401);
        }
        if(!Hash::check($credentials['password'], $user->password)){
            return response()->json([
                'Erro' => 'Senha invalida!'
            ], 401);
        }
        $token = JWTAuth::fromUser($user);
        $objectToken = JWTAuth::setToken($token);
        $user->token = $token;
        $user->save();
        $expiracao = JWTAuth::decode($objectToken->getToken())->get('exp');
        $expiracao = date('d/m/Y H:m:s ',$expiracao);
        return response()->json([
            'token_acesso' => $token,
            'expires_in' => $expiracao,
            'user'=>$user['name'],
            'userid'=>$user['userid'],
            'personid'=>$user['personid'],
            'Status'=>true
        ]);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }
}
