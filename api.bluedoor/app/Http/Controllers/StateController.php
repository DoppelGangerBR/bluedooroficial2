<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
class StateController
{
    public function index(){
        return State::all();
    }

    public function statesInCountry(Request $request){
        $countryid = $request->header('countryId');
        return State::where('countryid',$countryid)->get();
    }
}
