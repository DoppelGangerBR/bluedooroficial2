<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
class PersonController
{
    //
    public function updatePerson(Request $request){
        $personData = $request->all();
        $personUpdated = Person::update($personData);
        if($personUpdated){
            return response()->json(['Status'=>true]);
        }else{
            return response()->json(['Status'=>false]);
        }
    }
}
