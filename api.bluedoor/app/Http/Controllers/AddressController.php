<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
class AddressController extends Controller
{
    //
    public function store(Request $request){
        $newAddress = $request->all();
        $address = Address::create($newAddress);
        if($newAddress){
            return response()->json(['Status'=>true]);
        }else{
            return response()->json(['Status'=>false]);
        }
    }

    public function update(Request $request){
        $updatedAddress = $request->all();
        $address = Address::where($updatedAddress['id'],'=','addressid')->update($updatedAddress);
        if($newAddress){
            return response()->json(['Status'=>true]);
        }else{
            return response()->json(['Status'=>false]);
        }
    }
}
