<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;
class EmailController
{
    public function store(Request $request){
        $newEmail = $request->all();
        $email = Email::update($newEmail);
        if($email){
            return response()->json(['Status'=>true]);
        }else{
            return response()->json(['Status'=>false]);
        }
    }

    public function update(Request $request){
        $updatedEmail = $request->all();
        $email = Email::where($updatedEmail['id'],'=','emailid')->update($updatedEmail);
        if($email){
            return response()->json(['Status'=>true]);
        }else{
            return response()->json(['Status'=>false]);
        }
    }
}
