<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
class CountryController
{
    public function index(){
        return Country::all();
    }
    public function createCountryStateCityData(Request $request){
        $data = $request->all();
        return count($data);
    }
}
