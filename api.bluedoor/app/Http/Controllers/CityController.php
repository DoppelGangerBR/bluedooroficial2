<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
class CityController
{
    //
    public function index(Request $request){
        $CodEstado = $request->header('CodEstado');
        if($CodEstado == null){
            return City::all();
        }
        return City::where('CodEstado','=',$CodEstado)->get();
    }
}
