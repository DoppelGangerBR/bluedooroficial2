<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    //
    protected $fillable = ['emailid', 'email', 'created_at', 'updated_at', 'personid', 'isverified', 'verifiedat'];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'emails';
    protected $primaryKey = 'emailid';
    public function Person(){
        return $this->hasMany('App\Email');
    }
}
