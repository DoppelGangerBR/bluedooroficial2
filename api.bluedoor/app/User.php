<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;

    protected $fillable = ['userid', 'email', 'password', 'token', 'created_at', 'updated_at', 'personid','status'];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'users';
    protected $primaryKey = 'userid';
    public function User(){
        return $this->hasMany('App\User');
    }
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
