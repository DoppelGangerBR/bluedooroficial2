<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable = ['imageid', 'updated_at', 'imagename', 'roomid', 'created_at'];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'images';

    public function Person(){
        return $this->hasMany('App\Image');
    }
}
