<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    protected $fillable = ["id", "CodigoUf", "Nome", "Uf", "Regiao"];
    protected $table = 'estado';

    public function Person(){
        return $this->hasMany('App\State');
    }
}