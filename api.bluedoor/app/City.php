<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $fillable = ["id", "CodigoUf", "Nome", "Uf", "Regiao"];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'municipio';

    public function City(){
        return $this->hasMany('App\City');
    }
}
