<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    //
    protected $fillable = ['contactid', 'type', 'number', 'updated_at', 'created_at', 'personid'];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'contacts';

    public function Person(){
        return $this->hasMany('App\Country');
    }
}
