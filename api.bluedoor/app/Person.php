<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    //
    protected $fillable = ['personid', 'cnjpcpf', 'name', 'created_at', 'updated_at', 'surname', 'isenabled', 'persontype', 'profileimage'];
    protected $dates = [ 'created_at', 'updated_at'];
    protected $table = 'persons';
    protected $primaryKey = 'personid';
    public function Person(){
        return $this->hasMany('App\Person');
    }
}
