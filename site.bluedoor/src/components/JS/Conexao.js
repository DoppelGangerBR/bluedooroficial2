var protocolo = location.protocol;
var url = protocolo+"//192.168.2.107";
var porta = '8090';
    export function post(rota,objeto){
        return new Promise((resolve,reject) =>{
            var request = new XMLHttpRequest();
            var json = JSON.stringify(objeto);
            if(protocolo == 'http:'){
                request.open('POST', url+':'+porta+'/api/'+rota,true);
            }else{
                console.log(url+'/api/public/api/'+rota);
                request.open('POST', url+'/api/public/api/'+rota,true);
            }
            request.setRequestHeader('Content-Type','Application/json');
            try{
                request.send(json);
                request.onreadystatechange = function(){
                    if(request.readyState == request.DONE){
                        if(request.status == 200){
                            resolve(request.response);
                        }else{
                            reject(request.response);
                        }
                    }
                }
            }catch(err){
                reject(err);
            }
        });
    }
    export function get(rota){
        return new Promise((resolve,reject) =>{
            var request = new XMLHttpRequest();
            if(protocolo == 'http:'){
                request.open('GET', url+':'+porta+'/api/'+rota,true);
            }else{
                request.open('GET', url+'/api/public/api/'+rota,true);
            }
            request.setRequestHeader('Content-Type','Application/json');
            try{
                request.send();
                request.onreadystatechange = function(){
                    if(request.readyState == request.DONE){
                        if(request.status == 200){
                            resolve(request.response);
                        }
                        reject(request.response);
                    }
                }
            }catch(err){
                reject(err);
            }
        });
    }
    
    
    export function getComCabecalho(rota,cabecalho,valor){
        return new Promise((resolve,reject) =>{
            var request = new XMLHttpRequest();
            if(protocolo == 'http:'){
                request.open('GET', url+':'+porta+'/api/'+rota,true);
            }else{
                request.open('GET', url+'/api/public/api/'+rota,true);
            }
            request.setRequestHeader('Content-Type','Application/json');
            request.setRequestHeader(cabecalho,valor);
            try{
                request.send();
                request.onreadystatechange = function(){
                    if(request.readyState == request.DONE){
                        if(request.status == 200){
                            resolve(request.response);
                        }
                        reject(request.response);
                    }
                }
            }catch(err){
                reject(err);
            }
        });
    }