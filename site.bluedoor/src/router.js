import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import CadastroUsuario from './views/CadastroUsuario.vue'
import VueRouter from 'vue-router'
Vue.use(Router)

/*export default new Router({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      name:'Login',
      component:Login,
      path:'/auth/login'
    }
  ]
})*/

export default new Router({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      name:'Login',
      component:Login,
      path:'/auth/login'
    },
    {
      name:'Cadastro de usuário',
      component:CadastroUsuario,
      path:'/auth/registro'
    }
  ],
  mode:'history',
  base:'/'
})

