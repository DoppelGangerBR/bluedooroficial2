
CREATE SEQUENCE public.countries_contryid_seq;

CREATE TABLE public.Countries (
                ContryId INTEGER NOT NULL DEFAULT nextval('public.countries_contryid_seq'),
                CountryName VARCHAR NOT NULL,
                CONSTRAINT contryid PRIMARY KEY (ContryId)
);


ALTER SEQUENCE public.countries_contryid_seq OWNED BY public.Countries.ContryId;

CREATE SEQUENCE public.states_stateid_seq_2;

CREATE TABLE public.States (
                StateId INTEGER NOT NULL DEFAULT nextval('public.states_stateid_seq_2'),
                ContryId INTEGER NOT NULL,
                StateName VARCHAR NOT NULL,
                CONSTRAINT stateid PRIMARY KEY (StateId)
);


ALTER SEQUENCE public.states_stateid_seq_2 OWNED BY public.States.StateId;

CREATE SEQUENCE public.cities_cityid_seq;

CREATE TABLE public.Cities (
                CityId INTEGER NOT NULL DEFAULT nextval('public.cities_cityid_seq'),
                ZipCode VARCHAR NOT NULL,
                NomeCidade VARCHAR NOT NULL,
                StateId INTEGER NOT NULL,
                CONSTRAINT cityid PRIMARY KEY (CityId)
);


ALTER SEQUENCE public.cities_cityid_seq OWNED BY public.Cities.CityId;

CREATE SEQUENCE public.persons_personid_seq;

CREATE TABLE public.Persons (
                PersonId INTEGER NOT NULL DEFAULT nextval('public.persons_personid_seq'),
                CNPJ VARCHAR(350) NOT NULL,
                CPF VARCHAR NOT NULL,
                Name VARCHAR(350) NOT NULL,
                created_at TIMESTAMP NOT NULL,
                updated_at TIMESTAMP NOT NULL,
                Surname VARCHAR(350) NOT NULL,
                IsEnabled BOOLEAN DEFAULT true NOT NULL,
                PersonType INTEGER NOT NULL,
                ProfileImage VARCHAR NOT NULL,
                CONSTRAINT personid PRIMARY KEY (PersonId)
);


ALTER SEQUENCE public.persons_personid_seq OWNED BY public.Persons.PersonId;

CREATE SEQUENCE public.rooms_roomid_seq;

CREATE TABLE public.Rooms (
                RoomId INTEGER NOT NULL DEFAULT nextval('public.rooms_roomid_seq'),
                PersonId INTEGER NOT NULL,
                Details VARCHAR NOT NULL,
                created_at TIMESTAMP NOT NULL,
                updated_at TIMESTAMP NOT NULL,
                Address VARCHAR NOT NULL,
                Number VARCHAR NOT NULL,
                ContryId INTEGER NOT NULL,
                Neighborhood VARCHAR NOT NULL,
                Complement VARCHAR NOT NULL,
                StateId INTEGER NOT NULL,
                CityId INTEGER NOT NULL,
                ZipCode VARCHAR NOT NULL,
                CONSTRAINT roomid PRIMARY KEY (RoomId)
);


ALTER SEQUENCE public.rooms_roomid_seq OWNED BY public.Rooms.RoomId;

CREATE SEQUENCE public.images_imageid_seq;

CREATE TABLE public.Images (
                ImageId INTEGER NOT NULL DEFAULT nextval('public.images_imageid_seq'),
                updated_at TIMESTAMP NOT NULL,
                ImageName VARCHAR NOT NULL,
                RoomId INTEGER NOT NULL,
                created_at TIMESTAMP NOT NULL,
                CONSTRAINT imageid PRIMARY KEY (ImageId)
);


ALTER SEQUENCE public.images_imageid_seq OWNED BY public.Images.ImageId;

CREATE SEQUENCE public.adresses_adressesid_seq;

CREATE TABLE public.Adresses (
                adressesId INTEGER NOT NULL DEFAULT nextval('public.adresses_adressesid_seq'),
                CityId INTEGER NOT NULL,
                PersonId INTEGER NOT NULL,
                ZipCode VARCHAR NOT NULL,
                Address VARCHAR NOT NULL,
                Number VARCHAR NOT NULL,
                Neighborhood VARCHAR NOT NULL,
                Complement VARCHAR NOT NULL,
                CONSTRAINT adressesid PRIMARY KEY (adressesId)
);


ALTER SEQUENCE public.adresses_adressesid_seq OWNED BY public.Adresses.adressesId;

CREATE SEQUENCE public.contacts_contactid_seq;

CREATE TABLE public.Contacts (
                ContactId INTEGER NOT NULL DEFAULT nextval('public.contacts_contactid_seq'),
                Type INTEGER NOT NULL,
                Number VARCHAR NOT NULL,
                updated_at TIMESTAMP NOT NULL,
                created_at TIMESTAMP NOT NULL,
                PersonId INTEGER NOT NULL,
                CONSTRAINT contactid PRIMARY KEY (ContactId)
);


ALTER SEQUENCE public.contacts_contactid_seq OWNED BY public.Contacts.ContactId;

CREATE SEQUENCE public.emails_emailid_seq;

CREATE TABLE public.Emails (
                EmailId INTEGER NOT NULL DEFAULT nextval('public.emails_emailid_seq'),
                Email VARCHAR NOT NULL,
                created_at TIMESTAMP NOT NULL,
                updated_at TIMESTAMP NOT NULL,
                PersonId INTEGER NOT NULL,
                CONSTRAINT idemail PRIMARY KEY (EmailId, Email)
);


ALTER SEQUENCE public.emails_emailid_seq OWNED BY public.Emails.EmailId;

CREATE SEQUENCE public.users_userid_seq;

CREATE TABLE public.Users (
                UserId VARCHAR NOT NULL DEFAULT nextval('public.users_userid_seq'),
                Username VARCHAR NOT NULL,
                Password VARCHAR NOT NULL,
                Token VARCHAR(500) NOT NULL,
                created_at TIMESTAMP NOT NULL,
                updated_at TIMESTAMP NOT NULL,
                PersonId INTEGER NOT NULL,
                CONSTRAINT userid PRIMARY KEY (UserId)
);


ALTER SEQUENCE public.users_userid_seq OWNED BY public.Users.UserId;

ALTER TABLE public.States ADD CONSTRAINT countries_states_fk
FOREIGN KEY (ContryId)
REFERENCES public.Countries (ContryId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Rooms ADD CONSTRAINT countries_rooms_fk
FOREIGN KEY (ContryId)
REFERENCES public.Countries (ContryId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Cities ADD CONSTRAINT states_cities_fk
FOREIGN KEY (StateId)
REFERENCES public.States (StateId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Rooms ADD CONSTRAINT states_rooms_fk
FOREIGN KEY (StateId)
REFERENCES public.States (StateId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Adresses ADD CONSTRAINT cities_adresses_fk
FOREIGN KEY (CityId)
REFERENCES public.Cities (CityId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Rooms ADD CONSTRAINT cities_rooms_fk
FOREIGN KEY (CityId)
REFERENCES public.Cities (CityId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Users ADD CONSTRAINT pessoas_users_fk
FOREIGN KEY (PersonId)
REFERENCES public.Persons (PersonId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Emails ADD CONSTRAINT pessoas_emails_fk
FOREIGN KEY (PersonId)
REFERENCES public.Persons (PersonId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Contacts ADD CONSTRAINT pessoas_contacts_fk
FOREIGN KEY (PersonId)
REFERENCES public.Persons (PersonId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Adresses ADD CONSTRAINT persons_adresses_fk
FOREIGN KEY (PersonId)
REFERENCES public.Persons (PersonId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Rooms ADD CONSTRAINT persons_rooms_fk
FOREIGN KEY (PersonId)
REFERENCES public.Persons (PersonId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Images ADD CONSTRAINT rooms_images_fk
FOREIGN KEY (RoomId)
REFERENCES public.Rooms (RoomId)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
